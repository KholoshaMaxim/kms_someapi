<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 04.02.18
 * Time: 12:39
 */

class KMS_SomeApi_Block_Myproductlist extends Mage_Core_Block_Template
{
    public function getMyProducts()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*');
        $collection->getSelect()->limit(1);

        return $collection;
    }
}