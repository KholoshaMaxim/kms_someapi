<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.02.18
 * Time: 21:04
 */

class KMS_SomeApi_Model_Resource_SomeApi extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('someapi/bearer', 'id');
    }
}
