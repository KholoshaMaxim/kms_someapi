<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.02.18
 * Time: 21:16
 */

class KMS_SomeApi_Model_Resource_Bearer_Colletions extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('someapi/bearer');
    }
}