<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 03.02.18
 * Time: 21:25
 */

$this->startSetup();

try {
    $table = $this->getConnection()
        ->newTable($this->getTable('someapi/bearer'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true,
        ))
        ->addColumn('bearer', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
            'nullable' => false
        ));

    $this->getConnection()->createTable($table);
} catch (\Zend_Db_Exception $ex)
{
    var_dump($ex);
}

$this->endSetup();